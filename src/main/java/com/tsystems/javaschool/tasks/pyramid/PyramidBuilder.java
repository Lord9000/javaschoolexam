package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
            int mid = getCountRowPiramid(inputNumbers.size());
            int[][] arr = new int[mid][mid * 2 - 1];
            int count = 1;
            for (int s = 0; s < inputNumbers.size(); ) {
                for (int i = 0, qpos = mid - (count); i < count; qpos += 2, i++, s++) {
                    arr[count - 1][qpos] = inputNumbers.get(s);
                }
                count++;
            }
            return arr;
        }catch (IndexOutOfBoundsException | NullPointerException | OutOfMemoryError e){
            throw new CannotBuildPyramidException();
        }
    }

    private int getCountRowPiramid(int size) {
        int count = 0;
        for (int i = 0; size > 0; i++) {
            size -= i;
            count++;
        }
        return --count;
    }
}
