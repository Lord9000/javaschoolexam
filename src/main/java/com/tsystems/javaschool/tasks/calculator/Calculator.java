package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    private static int GetPriority(char operation) {
        switch (operation) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            case '(':
            case ')':
                return 3;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static double GoAction(char operation, double first, double second) {
        switch (operation) {
            case '+':
                return first + second;
            case '-':
                return second - first;
            case '*':
                return first * second;
            case '/':
                if (first == 0)
                    throw new IllegalArgumentException("Argument 'divisor' is 0");
                return second / first;
        }
        return 0;
    }

    public static String calculator(String[] strArr) {
        Stack<Character> operators = new Stack<>();
        Stack<Double> operands = new Stack<>();
        try {
            for (int i = 0; i < strArr.length; i++) {
                switch (strArr[i]) {
                    case "(":
                        operators.push('(');
                        break;
                    case ")":
                        while (operators.peek() != '(') {
                            double right = operands.pop();
                            double left = operands.pop();
                            char operator = operators.pop();
                            operands.push(GoAction(operator, right, left));
                        }
                        operators.pop();
                        break;
                    case "+":
                    case "-": {
                        if (!operators.isEmpty() && GetPriority(operators.peek()) == GetPriority(strArr[i].charAt(0))) {
                            double result = GoAction(operators.pop(), operands.pop(), operands.pop());
                            operands.push(result);
                        }
                        operators.push(strArr[i].charAt(0));
                        break;
                    }
                    case "*":
                    case "/":
                        if (strArr[i + 1].charAt(0) == '(') {
                            operators.push(strArr[i].charAt(0));
                        } else {
                            char operation = strArr[i].charAt(0);
                            if (operators.isEmpty() || (operators.peek() != '*' && operators.peek() != '/')) {
                                i++;
                                double first = Double.parseDouble(strArr[i]);
                                operands.push(GoAction(operation, first, operands.pop()));
                            } else {
                                operands.push(GoAction(operators.pop(), operands.pop(), operands.pop()));
                                operators.push(operation);
                            }
                        }
                        break;
                    default:
                        operands.push(Double.parseDouble(strArr[i]));
                        break;
                }
            }
            double result;
            if (operators.isEmpty())
                result = operands.pop();
            else
                result = GoAction(operators.pop(), operands.pop(), operands.pop());
            if (!(operands.isEmpty() && operators.isEmpty()))
                return null;
            if (result == (int) result)
                return Integer.toString((int) result);
            return String.valueOf(result);
        } catch (EmptyStackException | IllegalArgumentException e) {
            return null;
        }
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null)
            return null;
        String[] strArr = statement.split("((?<=[+\\-*/()])|(?=[+\\-*/()]))");
        return calculator(strArr);
    }

}
