# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The solution is to be written using Java 1.8 only, external libraries are forbidden. 
You can add dependencies with scope "test" if it's needed to write new unit-tests.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 

[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Dan Minkin
* Codeship : [![Codeship Status for Lord9000/javaschoolexam](https://app.codeship.com/projects/f17fa190-15da-0138-de5b-66e921efcf73/status?branch=master)](https://app.codeship.com/projects/380833)

 

### How can I submit the result?  ###

* Make sure your code can be built and all tests are green (example command: "mvn clean install")
* Commit and push all changes to your repository
* Configure the build on CI server like Codeship
* Add build badge and your name to the README.md under Result section
* Check that the badge shows green build. We will not accept your solution if there is any red badge on the page.
* Send us an email with the link to your repository. Be aware that the build must be green all the time after you send us the link

### Who do I talk to? ###

* In case of any questions contact the person who sent you the task (first) or [HR department](mailto:job@t-systems.ru) of T-Systems RU (second).

### Useful links ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Codeship](https://codeship.com)